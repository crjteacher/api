var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');

var path = require('path');

var url_mlab = "https://api.mlab.com/api/1/databases/bdbanca2/collections/movimientos?apiKey=cH8HBIbvFWNmtVOklgzzFap2P8Ewpuij";

var clienteMLab = requestjson.createClient(url_mlab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/movimientos/", function (req, res) {
  clienteMLab.get('', function(err, resM, body) {
    if(err) {
      console.log(body);
    } else {
      console.log(body);
      res.send(body);
    }
  });
});

// Reto 1: Crear peticion POST

app.post("/movimientos", function(req, res) {
  clienteMLab.headers['Content-Type'] = 'application/json';
  console.log(req.body);
  clienteMLab.post('', req.body, function(err, resM, body) {
    if(err) {
      console.log(body);
    } else {
      res.send(body);
    }
  });
});


app.get("/clientes/:idcliente", function (req, res) {
  res.send("Cliente enviado: " + req.params.idcliente);
});

app.post("/", function(req, res) {
  res.send("Recibí el post actualizada");
});

app.delete("/", function(req, res) {
  res.send("{\"msg\": \"Elemento eliminado exitosamente\"}")
});

app.put("/", function(req, res) {
  res.send("{\"msg\": \"Elemento actualizado exitosamente\"}")
});
